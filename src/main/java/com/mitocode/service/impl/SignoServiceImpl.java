package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ISignoDao;
import com.mitocode.model.SignoVital;
import com.mitocode.service.ISignoService;


@Service
public class SignoServiceImpl implements ISignoService{

	@Autowired
	private ISignoDao dao;
	
	@Override
	public SignoVital registrar(SignoVital t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}
	
	@Override
	public SignoVital modificar(SignoVital t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
	}

	@Override
	public SignoVital listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<SignoVital> listar() {
		return dao.findAll();
	}

	

}
