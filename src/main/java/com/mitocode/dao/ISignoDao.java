package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.model.SignoVital;

public interface ISignoDao extends JpaRepository<SignoVital, Integer>{

}
