package com.mitocode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Examen;
import com.mitocode.model.SignoVital;
import com.mitocode.service.IExamenService;
import com.mitocode.service.ISignoService;

@RestController
@RequestMapping("/signos")
public class SignoController {

	@Autowired
	private ISignoService service;

	@GetMapping(produces = "application/json")
	public List<SignoVital> listar() {
		return service.listar();
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public SignoVital listarPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	@PostMapping(produces = "application/json", consumes = "application/json")
	public SignoVital registrar(@RequestBody SignoVital signo) {
		return service.registrar(signo);
	}
	
	@PutMapping(produces = "application/json", consumes = "application/json")
	public SignoVital modificar(@RequestBody SignoVital signo) {
		return service.modificar(signo);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
	}
}
